/***************************************************************************
  This is a library for the BME280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME280 Breakout
  ----> http://www.adafruit.com/products/2650

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface. The device's I2C address is either 0x76 or 0x77.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
  See the LICENSE file for details.
 ***************************************************************************/

#define PRINT
#define GCP 

#include "main.h"
#include "setup.h"
#include "telemetry.h"


CloudIoTCoreDevice *device;
Client *netClient;
MQTTClient *mqttClient;
CloudIoTCoreMqtt *mqtt;
Adafruit_BME280 bme;

unsigned long delayTime;
float sensorValue;
unsigned long iat;
String json;
unsigned long jwtExpiration;

String sensorId = String(SENSOR_IDENTIFIER);

void setup() {

#ifdef PRINT
    Serial.begin(115200);
#endif /* PRINT */ 

    while(!Serial);    // time to get serial running

#ifdef PRINT
    Serial.println("Begin startup");
#endif /* PRINT */

    setupGpio();
    digitalWrite(LED_RED, HIGH);
    setupWifi();
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_YELLOW, HIGH);
    startupSensor();
    digitalWrite(LED_YELLOW, LOW);

#ifdef GCP
    digitalWrite(LED_BLUE, HIGH);
    setupCloudIoT();
    digitalWrite(LED_BLUE, LOW);
#endif /* GCP */

#ifdef PRINT
    Serial.println("Startup complete");
#endif /* PRINT */

    digitalWrite(LED_GREEN, HIGH);

}


void updateValues() {

  json = "{\"identifier\":\"" + sensorId + "\", \"timestamp\":\"" + String(time(nullptr)) + "\"";
#ifdef PRINT
    Serial.print("Temperature = ");
#endif /* PRINT */
    sensorValue = bme.readTemperature();

#ifdef GCP
    json += ",\"temperature\":\"" + String(sensorValue) + "\"";
#endif /* GCP */

#ifdef PRINT
    Serial.print(sensorValue);
    Serial.println(" *C");
#endif /* PRINT */

    sensorValue = bme.readPressure() / 100.0F;

#ifdef GCP
    json += ",\"pressure\":\"" + String(sensorValue) + "\"";
#endif /* GCP */

#ifdef PRINT
    Serial.print("Pressure = ");
    Serial.print(sensorValue);
    Serial.println(" hPa");
#endif /* PRINT */

    // Serial.print("Approx. Altitude = ");
    // Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    // Serial.println(" m");

    sensorValue = bme.readHumidity();

#ifdef GCP
    json += ",\"humidity\":\"" + String(sensorValue) + "\"";
#endif /* GCP */
  
#ifdef PRINT
    Serial.print("Humidity = ");
    Serial.print(sensorValue);
    Serial.println(" %");

    Serial.println();
#endif /* PRINT */

#ifdef GCP
    json += "}";
    Serial.println(json);
    publishTelemetry(json);
#endif /* GCP */

}

void loop() { 
    mqtt->loop();
    delay(10);  // <- fixes some issues with WiFi stability

  if (!mqttClient->connected()) {
    mqtt->mqttConnect();    
  }


    // if (jwtExpiration - time(nullptr) <= JWT_REFRESH_THRESHOLD_SECONDS) {
    //     Serial.println("############# Should update jwt!!!!");
    //     getJwt();
    // }

    // Serial.printf("Time %s", String(time(nullptr)));

    updateValues();
    delay(delayTime);
    digitalWrite(RESET, LOW);
}

void messageReceived(String &topic, String &payload){
  Serial.println("incoming: " + topic + " - " + payload);
}