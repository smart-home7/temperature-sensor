#include "main.h"
#include "telemetry.h"

bool publishTelemetry(String subtopic, float value) {
  return mqtt->publishTelemetry(subtopic, String(value));
}

bool publishTelemetry(String data){
  return mqtt->publishTelemetry(data);
}

bool publishTelemetry(float value){
  return mqtt->publishTelemetry(String(value));
}

bool publishState(String data) {
  return mqtt->publishState(data);
}