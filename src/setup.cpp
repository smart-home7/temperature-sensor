#include <WiFi.h>
#include <WiFiClientSecure.h>

#include "main.h"
#include "setup.h"
#include "telemetry.h"

// SDA‎: ‎SDA (default is GPIO 21)
// VCC‎: ‎usually 3.3V or 5V
// SCL‎: ‎SCL (default is GPIO 22)

extern unsigned long jwtExpiration;

void setupWifi(){
  Serial.println("Starting wifi");
  Serial.print(WIFI_SSID);
  WiFi.mode(WIFI_STA);
  // WiFi.setSleep(false); // May help with disconnect? Seems to have been removed from WiFi
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("Connecting to WiFi");
  uint8_t wifiWaitingCounter = 0;
  while (WiFi.status() != WL_CONNECTED){
    delay(100);
    wifiWaitingCounter++;
    if (wifiWaitingCounter >= 10) {
      ESP.restart();
    }
  }

  configTime(0, 0, NTP_PRIMARY_DOMAIN, NTP_SECONDARY_DOMAIN);
  Serial.println("Waiting on time sync...");
  while (time(nullptr) < 1510644967){
    delay(10);
  }
}

void startupSensor() {
    Serial.println(F("BME280 test"));

    unsigned status;
    
    // default settings
    status = bme.begin(0x76);  
    // You can also pass in a Wire library object like &Wire2
    // status = bme.begin(0x76, &Wire2)
    if (!status) {
        Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
        Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(),16);
        Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
        Serial.print("   ID of 0x56-0x58 represents a BMP 280,\n");
        Serial.print("        ID of 0x60 represents a BME 280.\n");
        Serial.print("        ID of 0x61 represents a BME 680.\n");
        while (1) delay(10);
    }
    
    Serial.println("-- Default Test --");
    delayTime = 1000 * PUBLISH_INTERVAL_SEC;

    Serial.println();
}

String getJwt() {
  iat = time(nullptr);
  unsigned long validityPeriod = 60 * JWT_VALIDITY_MINUTES; 
  jwtExpiration = iat + validityPeriod;
  Serial.print("IAT: ");
  Serial.println(iat);
  Serial.print("EXP: ");
  Serial.println(jwtExpiration);
  Serial.print("JWT: ");
  String jwt = device->createJWT(iat, 60 * JWT_VALIDITY_MINUTES);
  Serial.println(jwt);
  return jwt;
}

void setupCloudIoT(){
  device = new CloudIoTCoreDevice(
      PROJECT_ID, LOCATION, REGISTRY_ID, DEVICE_ID, privateKey);
  netClient = new WiFiClientSecure();
  mqttClient = new MQTTClient(512);
  mqttClient->setOptions(180, true, 1000); // keepAlive, cleanSession, timeout
  mqtt = new CloudIoTCoreMqtt(mqttClient, netClient, device);
  mqtt->setUseLts(true);
  mqtt->startMQTT();
  mqtt->mqttConnect();
}

void setupGpio() {
  pinMode(LED_BLUE,OUTPUT);
  pinMode(LED_RED,OUTPUT);
  pinMode(LED_YELLOW,OUTPUT);
  pinMode(LED_GREEN,OUTPUT);
  pinMode(RESET, OUTPUT);
  digitalWrite(RESET, HIGH);

  digitalWrite(LED_BLUE, HIGH);
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_YELLOW, HIGH);
  digitalWrite(LED_GREEN, HIGH);

  delay(3000);

  digitalWrite(LED_BLUE, LOW);
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
  digitalWrite(LED_GREEN, LOW);
}

